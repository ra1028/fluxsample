//
//  PagenationResponse.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/17/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import Alembic

protocol PagenationResponse {
    associatedtype Item: Distillable
    
    init(items: [Item], totalCount: Int, page: Int, hasNext: Bool)
}

struct GitHubResponse<Item: Distillable>: PagenationResponse {
    let items: [Item]
    let totalCount: Int
    let page: Int
    let hasNext: Bool
}
