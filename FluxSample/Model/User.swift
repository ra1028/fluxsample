//
//  User.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/17/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import Alembic

struct User {
    let id: Int
    let name: String
    let avatarUrl: URL
    let url: URL
}

extension User: Distillable {
    static func distil(json j: JSON) throws -> User {
        return try .init(
            id: j.distil("id"),
            name: j.distil("login"),
            avatarUrl: j.distil("avatar_url").flatMap(URL.init),
            url: j.distil("html_url").flatMap(URL.init)
        )
    }
}
