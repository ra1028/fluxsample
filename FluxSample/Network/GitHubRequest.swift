//
//  GitHubRequest.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/16/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import APIKit
import Alembic

protocol GitHubRequest: Request {
    func response(from j: JSON, urlResponse: HTTPURLResponse) throws -> Response
}

extension GitHubRequest {
    var baseURL: URL {
        return URL(string: Config.env.gitHub.apiBaseUrl)!
    }
    
    var headerFields: [String: String] {
        return [
            "Authorization": "token \(Config.env.gitHub.apiToken)",
            "Content-Type" : "application/json; charset=utf-8",
            "Accept" : "application/vnd.github.v3+json"
        ]
    }
    
    func response(from object: Any, urlResponse: HTTPURLResponse) throws -> Response {
        return try response(from: .init(object), urlResponse: urlResponse)
    }
}
