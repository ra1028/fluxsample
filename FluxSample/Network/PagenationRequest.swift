//
//  PagenationRequest.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/17/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import APIKit
import Alembic
import WebLinking

protocol PagenationRequest: GitHubRequest {
    associatedtype Response: PagenationResponse
    
    var page: Int { get }
}

extension PagenationRequest {
    func response(from j: JSON, urlResponse: HTTPURLResponse) throws -> Response {
        let lastURI = urlResponse.findLink(relation: "last")?.uri
        let queryItems = lastURI.flatMap(URLComponents.init)?.queryItems
        let hasNext = queryItems?
            .filter { $0.name == "page" }
            .first
            .flatMap { $0.value }
            .flatMap { Int($0) }
            .map { $0 > page }
            ?? false
        
        return try .init(
            items: j.distil("items"),
            totalCount: j.distil("total_count"),
            page: page,
            hasNext: hasNext
        )
    }
}
