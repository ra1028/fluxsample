//
//  SearchUserRequest.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/16/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import APIKit

struct SearchUserRequest: PagenationRequest {
    typealias Response = GitHubResponse<User>

    enum Sort: String {
        case followers, repositories, joined
    }
    
    enum Order: String {
        case asc, desc
    }
    
    let path = "/search/users"
    
    let method = HTTPMethod.get
    
    var queryParameters: [String : Any]? {
        var parameters: [String: Any] = [
            "q": query,
            "order": order.rawValue,
            "page": page,
            "per_page": perPage
        ]
        parameters["sort"] = sort?.rawValue
        return parameters
    }
    
    let query: String
    let order: Order
    let sort: Sort?
    let page: Int
    let perPage: Int
    
    init(query: String,
         order: Order = .desc,
         sort: Sort? = nil,
         page: Int,
         perPage: Int = 30) {
        self.query = query
        self.sort = sort
        self.order = order
        self.page = page
        self.perPage = perPage
    }
}
