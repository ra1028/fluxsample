//
//  UIViewControllerExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/19/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit
import RxSwift

extension Reactive where Base: UIViewController {
    var viewWillAppear: Observable<()> {
        return sentMessage(#selector(Base.viewWillAppear(_:))).map { _ in }
    }
}
