//
//  UITableViewCellExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/18/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit
import RxSwift

extension Reactive where Base: UITableViewCell {
    var prepareForReuse: Observable<Void> {
        return sentMessage(#selector(Base.prepareForReuse)).map { _ in }
    }
}
