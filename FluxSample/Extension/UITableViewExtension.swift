//
//  UITableViewExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/18/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit
import RxSwift

extension Reactive where Base: UITableView {
    func items<S: Sequence, Cell: UITableViewCell, O : ObservableType>
        (cellType: Cell.Type = Cell.self)
        -> (_ source: O)
        -> (_ configureCell: @escaping (Int, S.Iterator.Element, Cell) -> Void)
        -> Disposable
        where O.E == S {
            return items(cellIdentifier: .init(describing: Cell.self), cellType: cellType)
    }
}

extension UITableView {
    func register<Cell: UITableViewCell>(cellClass: Cell.Type) {
        register(cellClass, forCellReuseIdentifier: .init(describing: Cell.self))
    }
    
    func registerNib<Cell: UITableViewCell>(cellClass: Cell.Type) {
        let cellName = String(describing: Cell.self)
        register(UINib(nibName: cellName, bundle: .main), forCellReuseIdentifier: cellName)
    }
}
