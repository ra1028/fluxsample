//
//  SessionExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/17/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import APIKit
import RxSwift

extension Session: ReactiveCompatible {}

extension Reactive where Base: Session {
    static func send<Request: APIKit.Request>(_ request: Request) -> Observable<Request.Response> {
        return .create { observer in
            let task = Base.send(request, callbackQueue: .dispatchQueue(.global(qos: .default))) { result in
                switch result {
                case let .success(response):
                    observer.onNext(response)
                    observer.onCompleted()
                case let .failure(.connectionError(error))
                    where (error as NSError).code == URLError.cancelled.rawValue:
                    observer.onCompleted()
                case let .failure(error):
                    observer.onError(error)
                }
            }
            return Disposables.create { task?.cancel() }
        }
    }
}
