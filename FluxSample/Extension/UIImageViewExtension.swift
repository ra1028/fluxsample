//
//  UIImageViewExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/18/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

extension Reactive where Base: UIImageView {
    func setImage(with url: URL, placeholder: UIImage? = nil, options: KingfisherOptionsInfo = [.transition(.fade(0.2))]) -> Observable<Void> {
        return .create { [weak base = self.base] observer in
            guard let base = base else {
                observer.onCompleted()
                return Disposables.create()
            }
            let imageView: UIImageView = base
            let task = imageView.kf.setImage(with: url, placeholder: placeholder, options: options) { _, error, _, _ in
                if let error = error, error.code != URLError.cancelled.rawValue {
                    observer.onError(error)
                    return
                }
                observer.onNext()
                observer.onCompleted()
            }
            return Disposables.create(with: task.cancel)
        }
    }
}
