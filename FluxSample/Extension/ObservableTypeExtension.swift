//
//  ObservableTypeExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/19/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift

extension ObservableType {
    func subscribe(with disposable: @escaping (E) -> Disposable) -> Disposable {
        return flatMap { Observable.never().startWith($0).do(onDispose: disposable($0).dispose) }.subscribe()
    }
}
