//
//  UIScrollViewExtension.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/19/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift

extension Reactive where Base: UIScrollView {
    func isNearBottom(offset: CGFloat) -> Observable<Bool> {
        return contentOffset
            .map { [weak base = self.base] contentOffset in
                guard let base = base else { return false }
                
                let visibleHeight = base.frame.height - base.contentInset.top - base.contentInset.bottom
                let y = contentOffset.y + base.contentInset.top
                let threshold = max(0, base.contentSize.height - visibleHeight + offset)
                return y > threshold
            }
            .distinctUntilChanged()
    }
}
