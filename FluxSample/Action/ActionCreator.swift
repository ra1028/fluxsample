//
//  ActionCreator.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/15/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift

struct ActionCreator<Base: ActionCreatable> {
    fileprivate init() {}
}

// TODO: Allow struct or enum in Swift 3.1
protocol ActionCreatable: class {}

extension ActionCreatable {
    static var creator: ActionCreator<Self> {
        return .init()
    }
}
