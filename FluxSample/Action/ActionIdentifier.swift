//
//  ActionIdentifier.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/16/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift

protocol ActionIdentifier {
    associatedtype Payload
}

extension ActionIdentifier {
    static func dispatch(_ payload: @autoclosure @escaping () -> Payload) {
        Dispatcher.dispatch(Action<Self>(payload))
    }
    
    static func dispatch(_ payload: @escaping () -> Payload) {
        Dispatcher.dispatch(Action<Self>(payload))
    }
}
