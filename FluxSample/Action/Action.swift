//
//  Action.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/15/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift

final class Action<Identifier: ActionIdentifier> {
    private let createPayload: () -> Identifier.Payload
    
    var payload: Identifier.Payload {
        return createPayload()
    }
    
    init(_ payload: @autoclosure @escaping () -> Identifier.Payload) {
        self.createPayload = payload
    }
    
    init(_ createPayload: @escaping () -> Identifier.Payload) {
        self.createPayload = createPayload
    }
}
