//
//  SearchUserAction.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/16/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift
import APIKit

final class SearchUserAction: ActionCreatable {
    struct Search: ActionIdentifier { typealias Payload = GitHubResponse<User> }
    struct Loading: ActionIdentifier { typealias Payload = Bool }
    struct Error: ActionIdentifier { typealias Payload = Swift.Error }
}

extension ActionCreator where Base: SearchUserAction {
    func refresh(query: String) -> Disposable {
        return loadMore(query: query, page: 1)
    }
    
    func loadMore(query: String, page: Int) -> Disposable {
        Base.Loading.dispatch(true)
        let request = SearchUserRequest(query: query, page: page)
        return Session.rx.send(request)
            .timeout(10, scheduler: ConcurrentDispatchQueueScheduler(qos: .default))
            .retry(3)
            .subscribe(
                onNext: { Base.Search.dispatch($0) },
                onError: {
                    Base.Loading.dispatch(false)
                    Base.Error.dispatch($0)
                },
                onCompleted: {  Base.Loading.dispatch(false) }
        )
    }
}
