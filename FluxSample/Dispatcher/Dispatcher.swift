//
//  Dispatcher.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/15/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift
import RxCocoa

struct Dispatcher {
    private static var connectorForToken = [Token: Connector]()
    private static let lock = MutexLock()
    private static let queue = DispatchQueue(label: "com.ryo.flux_sample.dispatcher.queue.serial")
    
    private init() {}
    
    static func dispatch<Identifier: ActionIdentifier>(_ action: Action<Identifier>) {
        lock.lock()
        defer { lock.unlock() }
        
        let token = Token(for: Identifier.self)
        let connector = connectorForToken[token] as? ActionConnector<Identifier>
        connector?.dispatch(action)
    }
    
    static func bind<Identifier: ActionIdentifier, O: ObserverType>(_: Identifier.Type, to observer: O) -> Disposable where Identifier.Payload == O.E {
        lock.lock()
        defer { lock.unlock() }
        
        let token = Token(for: Identifier.self)
        let connector = connectorForToken[token] as? ActionConnector<Identifier> ?? .init(queue: queue)
        connectorForToken[token] = connector
        
        func removeConnectorIfNoObservers() {
            lock.lock()
            defer { lock.unlock() }
            
            guard !connector.hasObservers else { return }
            connectorForToken[token] = nil
        }
        
        return connector.payload
            .do(onDispose: removeConnectorIfNoObservers)
            .bindTo(observer)
    }
}

private protocol Connector {}

private final class ActionConnector<Identifier: ActionIdentifier>: Connector {
    private let subject = PublishSubject<Action<Identifier>>()
    let payload: Observable<Identifier.Payload>
    
    var hasObservers: Bool {
        return subject.hasObservers
    }
    
    init(queue: DispatchQueue) {
        payload = subject
            .observeOn(ConcurrentDispatchQueueScheduler(queue: queue))
            .map { $0.payload }
            .share()
    }
    
    func dispatch(_ action: Action<Identifier>) {
        subject.onNext(action)
    }
}

private final class MutexLock: NSLocking {
    private var mutex = pthread_mutex_t()
    
    init() {
        let result = pthread_mutex_init(&mutex, nil)
        precondition(result == 0, "Failed to initialize mutex with error \(result).")
    }
    
    deinit {
        let result = pthread_mutex_destroy(&mutex)
        precondition(result == 0, "Failed to destroy mutex with error \(result).")
    }
    
    func lock() {
        let result = pthread_mutex_lock(&mutex)
        precondition(result == 0, "Failed to lock \(self) with error \(result).")
    }
    
    func unlock() {
        let result = pthread_mutex_unlock(&mutex)
        precondition(result == 0, "Failed to unlock \(self) with error \(result).")
    }
}


private struct Token: Hashable {
    let hashValue: Int
    private let value: String
    
    init<Identifier: ActionIdentifier>(for _: Identifier.Type) {
        value = .init(reflecting: Identifier.self)
        hashValue = value.hashValue
    }
    
    static func == (lhs: Token, rhs: Token) -> Bool {
        return lhs.value == rhs.value
    }
}
