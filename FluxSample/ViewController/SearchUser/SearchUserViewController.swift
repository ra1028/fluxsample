//
//  SearchUserViewController.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/15/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit
import SafariServices
import RxSwift
import RxCocoa

final class SearchUserViewController: UIViewController {
    private let store = SearchUserStore()
    fileprivate let disposeBag = DisposeBag()
    
    private let refreshControl = UIRefreshControl()
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var foundCountLabel: UILabel!
    @IBOutlet private weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset.bottom = 10
        tableView.rowHeight = 80
        tableView.addSubview(refreshControl)
        tableView.registerNib(cellClass: SearchUserCell.self)
        
        store.users
            .asDriver { _ in .empty() }
            .do(onNext: { [unowned self] in
                self.tableView.isHidden = false
                self.errorLabel.isHidden = true
                self.tableView.separatorStyle = $0.isEmpty ? .none : .singleLine
            })
            .drive(tableView.rx.items(cellType: SearchUserCell.self)) { _, user, cell in
                cell.configure(with: user)
            }
            .addDisposableTo(disposeBag)
        
        store.foundCount
            .map { "\($0)/\($1)" }
            .asDriver { _ in .empty() }
            .drive(foundCountLabel.rx.text)
            .addDisposableTo(disposeBag)
        
        let isLoading = store.isLoading.asDriver(onErrorJustReturn: false)
        
        isLoading
            .drive(indicatorView.rx.isAnimating)
            .addDisposableTo(disposeBag)
        
        isLoading
            .drive(refreshControl.rx.refreshing)
            .addDisposableTo(disposeBag)
        
        store.error
            .asDriver { .just($0) }
            .drive(onNext: { [unowned self] _ in
                self.errorLabel.isHidden = false
                self.tableView.isHidden = true
            })
            .addDisposableTo(disposeBag)
        
        let searchButtonClicked = searchBar.rx.searchButtonClicked
            .do(onNext: { [unowned self] in self.searchBar.resignFirstResponder() })
        let refreshControlChanged = refreshControl.rx.controlEvent(.valueChanged).asObservable()
        
        Observable.of(searchButtonClicked, refreshControlChanged).merge()
            .map { [unowned self] in self.searchBar.text ?? "" }
            .subscribe(with: SearchUserAction.creator.refresh(query:))
            .addDisposableTo(disposeBag)
        
        rx.viewWillAppear
            .subscribe(onNext: { [unowned self] in
                guard let indexPath = self.tableView.indexPathForSelectedRow else { return }
                self.tableView.deselectRow(at: indexPath, animated: true)
            })
            .addDisposableTo(disposeBag)
        
        tableView.rx.modelSelected(User.self)
            .subscribe(onNext: { [unowned self] in
                let vc = SFSafariViewController(url: $0.url)
                self.present(vc, animated: true)
            })
            .addDisposableTo(disposeBag)
        
        tableView.rx.isNearBottom(offset: 5)
            .throttle(0.5, scheduler: ConcurrentDispatchQueueScheduler(qos: .default))
            .filter { [unowned self] in self.store.loadMoreEnabled && $0 }
            .map { [unowned self] _ in (self.searchBar.text ?? "", self.store.nextPage) }
            .subscribe(with: SearchUserAction.creator.loadMore(query:page:))
            .addDisposableTo(disposeBag)
    }
}
