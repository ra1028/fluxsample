//
//  Config.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/16/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

struct Config {
    static let env = Environment()
}

extension Config {
    struct Environment {
        let gitHub = GitHub()
    }
}

extension Config.Environment {
    struct GitHub {
        let apiToken = gitHubApiToken
        let apiBaseUrl = "https://api.github.com"
    }
}
