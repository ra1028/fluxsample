//
//  AppDelegate.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/15/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
