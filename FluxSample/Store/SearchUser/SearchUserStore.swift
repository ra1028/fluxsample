//
//  SearchUserStore.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/15/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import RxSwift

final class SearchUserStore {
    let users: Observable<[User]>
    let foundCount: Observable<(loaded: Int, total: Int)>
    let isLoading: Observable<Bool>
    let error: Observable<Error>
    
    private(set) var nextPage = 1
    private(set) var loadMoreEnabled = false
    
    private let searchSubject = PublishSubject<GitHubResponse<User>>()
    private let isLoadingSubject = PublishSubject<Bool>()
    private let errorSubject = PublishSubject<Error>()
    private let disposeBag = DisposeBag()
    
    init() {
        let search = searchSubject.scan([]) { $1.page == 1 ? [$1] : $0 + [$1] }
        users = search.map { $0.flatMap { $0.items } }
        foundCount = search.map { ($0.reduce(0) { $0 + $1.items.count }, $0.last?.totalCount ?? 0) }
        isLoading = isLoadingSubject.distinctUntilChanged()
        error = errorSubject
        
        searchSubject
            .map { $0.page + 1 }
            .subscribe(onNext: { [unowned self] in self.nextPage = $0 })
            .addDisposableTo(disposeBag)
        
        Observable.combineLatest(
            search.map { $0.last?.hasNext ?? false },
            foundCount.map { $0.0 > 0 },
            isLoading) { $0 && $1 && !$2 }
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] in self.loadMoreEnabled = $0 })
            .addDisposableTo(disposeBag)
        
        Dispatcher.bind(SearchUserAction.Search.self, to: searchSubject).addDisposableTo(disposeBag)
        Dispatcher.bind(SearchUserAction.Loading.self, to: isLoadingSubject).addDisposableTo(disposeBag)
        Dispatcher.bind(SearchUserAction.Error.self, to: errorSubject).addDisposableTo(disposeBag)
    }
}
