//
//  SearchUserCell.swift
//  FluxSample
//
//  Created by Ryo Aoyama on 12/19/16.
//  Copyright © 2016 Ryo Aoyama. All rights reserved.
//

import UIKit
import RxSwift

final class SearchUserCell: UITableViewCell {
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var urlLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    
    func configure(with user: User) {
        titleLabel.text = user.name
        urlLabel.text = user.url.absoluteString
        
        avatarImageView.rx.setImage(with: user.avatarUrl)
            .takeUntil(rx.prepareForReuse)
            .subscribe()
            .addDisposableTo(disposeBag)
    }
}
